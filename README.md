# Weather Warn

WeatherWarn is a python framework/script that utilizes Wunderground's
API to create triggers that perform actions based on certain criterion
being matched and or surpassed. Useful in the event of calling another
API that warns of a heatwarning, or enabling a GPIO connected device 
to perform a certain action when the trigger executes. 

# Author
The author of this project is [mailto:temet@teknik.io](temet@teknik.io). 
Please leave an issue or an email regarding this project if you have a
question or comment. 
